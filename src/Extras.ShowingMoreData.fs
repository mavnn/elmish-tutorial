module Extras.ShowingMoreData

open Prelude
open Elmish
open Fable.Helpers.React
open Fable.Helpers.React.Props
open Fulma
open Fulma.FontAwesome

let content =
    [ p_ """So: why does are Pokemon Model have lower case field names,
         when all of the rest of our records are upper case? Because,
         it turns out, the "fetchAs" function knows how to deserialize
         JSON to a record type as long as the names match exactly."""
      p_ """Have an other look at the PokeApi, and choose yourself some
         additional data to display with your Pokemon. You can even create
         hierarchies of records, and they'll be recursively deserialized."""
      p_ """For the full effect, check out the "sprites" sub-object on the
         Pokemon which contains urls of images. Try and get an image up and
         displayed.""" ]

let page =
    { Id = ExtrasShowingMoreData
      Title = "Showing More Data"
      Subtitle = None
      Content = content
      Requires = Set.ofList [ ExtrasMakeItPretty ] }

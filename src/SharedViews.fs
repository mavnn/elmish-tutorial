module SharedViews

open Prelude
open Elmish
open Fable.Helpers.React
open Fable.Helpers.React.Props
open Fulma
open Fulma.FontAwesome
open Update

let checkedIfComplete text isComplete =
    [ yield str text

      if isComplete then
          yield Text.span
                    [ Props [ ClassName "fa-stack has-text-weak-success"
                              Style [ AnimationName "checkAppears"
                                      AnimationDuration "1s"
                                      AnimationDirection "forwards"
                                      AnimationIterationCount "1" ] ] ]
                          [ Icon.stackChild [ Fa.Child.faStack1x
                                              Fa.Child.icon Fa.I.Circle ]
                            Icon.stackChild [ Fa.Child.faStack1x
                                              Fa.Child.icon Fa.I.Check
                                              Fa.Child.colorInverse ] ] ]

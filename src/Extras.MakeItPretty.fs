module Extras.MakeItPretty

open Prelude
open Elmish
open Fable.Helpers.React
open Fable.Helpers.React.Props
open Fulma
open Fulma.FontAwesome

let content =
    [ p_ """You'll have noticed that our view looks a bit like
         HTML - but that we seem to be using a bunch of element
         types that aren't part of the HTML specification like
         Containers and Levels."""
      p_ """These all come from a wonderful library called Fulma.
         Fulma is a type safe wrapper over the Bulma CSS library,
         allowing full access to it's powerful collection of layout
         and display tools."""
      p [] [ str """The documentation for Fulma is extremely good,
                 with F# example code for every type of component
                 included. Check out """

             a
                 [ Href
                       "https://mangelmaxime.github.io/Fulma/#fulma/layouts/level" ]
                 [ str "the level docs" ]
             str """, for example (and click on view code to, well,
                 view the code).""" ]
      p_ """Now have a go at rewriting your view using some of the
         Fulma components. Maybe make your Pokemon data into a table,
         or add a "Hero" to the top of your page.""" ]

let page =
    { Id = ExtrasMakeItPretty
      Title = "Make It Pretty"
      Subtitle = Some "Style and Safety"
      Content = content
      Requires = Set.ofList [ BeginningsLoad ] }

module Challenge.SharedUpdates

open Prelude
open Elmish
open Fable.Helpers.React
open Fable.Helpers.React.Props
open Fulma
open Fulma.FontAwesome

let content =
    [ p_ """Sometimes subcomponents have to be aware of changes
         within other components. In theory, subcomponents should
         expose messages which allow for this but it's not always
         the case."""
      p_ """Change the update method of your parent application so
         that if one Pokemon display is loading data, they all change
         to the loading state. You can go the "hack" route, and update
         the models of the subcomponents directly - or do it properly
         and change the Pokemon messages to allow for them to be set
         loading without actually firing a request."""
      p_ """In either case, you'll need to alter the update function
         of the parent application."""]

let page =
    { Id = ChallengeSharedUpdates
      Title = "Sharing Updates"
      Subtitle = None
      Content = content
      Requires = Set.ofList [ ChallengeMultipleSubs ] }

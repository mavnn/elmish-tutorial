module Prelude

open Elmish
open Fable.Helpers.React
open Fable.Helpers.React.Props
open Fulma
open Fulma.FontAwesome

type Languages =
    | FSharp
    | NoHighlight

let codeBlock lang (source : string) =
    let langClass =
        match lang with
        | FSharp -> "language-fsharp"
        | NoHighlight -> "language-none"
    pre [ Props.ClassName langClass ] [ code [] [ str <| source.Trim() ] ]

let fsharpBlock source = codeBlock FSharp source
let noneBlock source = codeBlock NoHighlight source
let p_ text = p [] [ str text ]

type PageId =
    | IntroPrereqs
    | IntroNewProjectFromTemplate
    | IntroPokeapi
    | BeginningsLoad
    | BeginningsDispatch
    | ExtrasMakeItPretty
    | ExtrasSubcomponents
    | ExtrasShowingMoreData
    | ChallengeMultipleSubs
    | ChallengeSubsCollections
    | ChallengeSharedUpdates
    | ChallengeWalkingTheTree
    | Zen

let allPages =
    Set.empty
    |> Set.add IntroPrereqs
    |> Set.add IntroNewProjectFromTemplate
    |> Set.add IntroPokeapi
    |> Set.add BeginningsLoad
    |> Set.add BeginningsDispatch
    |> Set.add ExtrasMakeItPretty
    |> Set.add ExtrasSubcomponents
    |> Set.add ExtrasShowingMoreData
    |> Set.add ChallengeMultipleSubs
    |> Set.add ChallengeSubsCollections
    |> Set.add ChallengeSharedUpdates
    |> Set.add ChallengeWalkingTheTree
    |> Set.add Zen

type Page =
    { Id : PageId
      Title : string
      Subtitle : string option
      Content : Fable.Import.React.ReactElement list
      Requires : Set<PageId> }

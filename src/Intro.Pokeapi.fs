module Intro.Pokeapi

open Prelude
open Elmish
open Fable.Helpers.React
open Fable.Helpers.React.Props
open Fulma
open Fulma.FontAwesome

let content =
    [ p [] [ str """We're going to be building the best Pokemon database UI
            in the whole world - ever! But first, given that we're
            not all Pokemon experts we'll have a look at where we're
            getting our data from. Fire up """
             a [ Href "https://pokeapi.co/" ] [ str "https://pokeapi.co" ]
             str """ and take a look around using the built in browser.""" ]

      p_
          """One request: please don't just sit on one Pokemon hitting refresh:
      each resource has a rate limit per IP accessing it, and there's a lot of us!""" ]

let page =
    { Id = IntroPokeapi
      Title = "Introducing Pokeapi"
      Subtitle = Some "Gotta load 'em all!"
      Content = content
      Requires = Set.empty }

module Intro.Prereqs

open Prelude
open Elmish
open Fable.Helpers.React
open Fable.Helpers.React.Props
open Fulma
open Fulma.FontAwesome

let content =
    [ p_ """We're working on the cutting edge of both the
           .NET ecosystem and front end development here, so
           we need a number of dependencies. Most will be installed
           by package managers in the project - but that means we
           need the package managers installed first!"""
      p_ "You'll need:"

      ul []
          [ li []
                [ str "An F# editor. Browse over to "
                  a [ Href "https://fsharp.org/" ] [ str "fsharp.org" ]
                  str " and select your operating system from the 'Use'"
                  str " menu if you don't have one installed already."]
            li []
                [ a [ Href "https://www.microsoft.com/net/download" ]
                      [ str "The .NET Core SDK for your operating system." ] ]

            li []
                [ a [ Href "https://nodejs.org/en/" ]
                      [ str "A recent version of node.js." ] ]

            li []
                [ a [ Href "https://yarnpkg.com/en/docs/install" ]
                      [ str
                            "The Yarn package manager for node.js (it's more reliable than npm)" ] ]

            li []
                [ a
                      [ Href
                            "https://www.mono-project.com/docs/getting-started/install/" ]
                      [ str "Mono if you're not on Windows." ]

                  str
                      " This will allow us to use the Paket package manager for .NET which has some features we rely on that Nuget does not support." ] ] ]

let page =
    { Id = IntroPrereqs
      Title = "Install these to get started"
      Subtitle = Some "Or: downloading the internet for fun and profit."
      Content = content
      Requires = Set.empty }

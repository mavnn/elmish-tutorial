module PageView

open Prelude
open Elmish
open Fable.Helpers.React
open Fable.Helpers.React.Props
open Fulma
open Fulma.FontAwesome
open SharedViews
open Update

let pageTitle page isComplete =
    [ yield Heading.h1 [ Heading.Is1 ] (checkedIfComplete page.Title isComplete)
      match page.Subtitle with
      | Some subTitle ->
          yield Heading.h4 [ Heading.IsSubtitle; Heading.Is4 ] [ str subTitle ]
      | None -> () ]

let contentSection page isComplete =
    Section.section [] [ Content.content [] [ yield! pageTitle page isComplete
                                              yield! page.Content ] ]

let markCompleteSection pageId isComplete dispatch =
    [ Section.section []
          [ Level.level []
                [ yield Level.left []
                            [ Level.item []
                                  [ Button.button
                                        [ Button.Color IsDanger

                                          Button.OnClick
                                              (fun _ -> dispatch (ClearProgress)) ]
                                        [ str "Clear all Progress" ] ] ]

                  if not isComplete then
                      yield Level.right []
                                [ Level.item []
                                      [ Button.button
                                            [ Button.Color IsSuccess

                                              Button.OnClick
                                                  (fun _ ->
                                                  dispatch (CompletePage pageId)) ]
                                            [ str "Finished!" ] ] ] ] ] ]

let view page isComplete dispatch =
    [ yield contentSection page isComplete
      yield! markCompleteSection page.Id isComplete dispatch ]

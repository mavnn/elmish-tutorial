module Challenge.WalkingTheTree

open Prelude
open Elmish
open Fable.Helpers.React
open Fable.Helpers.React.Props
open Fulma
open Fulma.FontAwesome

let content =
    [ p_ """PokeApi is a RESTful (mostly!) API, and that means
         hypertext. You'll notice that a lot of the fields of
         the Pokemon type you're downloading actually contain
         URLs rather than the full data of what they represent.
         The data in PokeApi is normalised, so anything that
         might be shared between multiple Pokemon is referenced
         as a separate resource with it's own URL."""
      p_ """Consider how you would load all of the "abilities" of
         a Pokemon after loading the main object. A subcomponent
         for abilities would be a good start, and you'll then
         want to build a collection of abilities in the Pokemon
         component."""
      p_ """When you got to the point of loading data, you should
         be able to repurpose the 'fetchPokemon' method (remember
         that you'll need the 'Fable.PowerPack' and
         'Fable.PowerPack.Fetch' namespaces open).""" ]

let page =
    { Id = ChallengeWalkingTheTree
      Title = "Walking the (HyperMedia) Tree"
      Subtitle = None
      Content = content
      Requires = Set.ofList [ ExtrasShowingMoreData
                              ChallengeSubsCollections ] }

module Model

open Prelude

type Model =
    { Value : string
      Completed : Set<PageId>
      CurrentPage : PageId }

let getPage pageId =
    match pageId with
    | IntroPrereqs -> Intro.Prereqs.page
    | IntroNewProjectFromTemplate -> Intro.NewProject.page
    | IntroPokeapi -> Intro.Pokeapi.page
    | BeginningsLoad -> Beginnings.Load.page
    | BeginningsDispatch -> Beginnings.Dispatch.page
    | ExtrasMakeItPretty -> Extras.MakeItPretty.page
    | ExtrasSubcomponents -> Extras.Subcomponents.page
    | ExtrasShowingMoreData -> Extras.ShowingMoreData.page
    | ChallengeMultipleSubs -> Challenge.MultipleSubs.page
    | ChallengeSubsCollections -> Challenge.SubsCollections.page
    | ChallengeSharedUpdates -> Challenge.SharedUpdates.page
    | ChallengeWalkingTheTree -> Challenge.WalkingTheTree.page
    | Zen -> Zen.page

module Challenge.MultipleSubs

open Prelude
open Elmish
open Fable.Helpers.React
open Fable.Helpers.React.Props
open Fulma
open Fulma.FontAwesome

let content =
    [ p_ """Now we have one subcomponent... but for a piece of
         code to be truly reusable, we need to be able to create
         independent instances of the component. It would be a
         bit of a blow to have only one DatePicker per page, for
         example."""
      p_ """Have a look at the parent application, and consider
         how you could create two, independent, Pokemon displays. You'll
         need a separate version of the model and a separate message
         wrapper for each display, to keep their data independent.""" ]

let page =
    { Id = ChallengeMultipleSubs
      Title = "Multiple Subcomponents"
      Subtitle = Some "Nuke 'em from orbit, they're multiplying!"
      Content = content
      Requires = Set.ofList [ ExtrasSubcomponents ] }

module Beginnings.Load

open Prelude
open Elmish
open Fable.Helpers.React
open Fable.Helpers.React.Props
open Fulma
open Fulma.FontAwesome

let content =
    [ p_ """Time to get more serious. To load and display a Pokemon, we'll
         need to rewrite basically all of the existing template app."""
      p_ """You'll probably have spotted from the Pokeapi that all Pokemon
      share some key traits. Let's pick out a couple of them (name, weight)
      and create a data structure to pull them out of the JSON the API
      provides (add this before the model type in "src/App.fs"):"""
      fsharpBlock """
type Pokemon =
    { name : string
      weight : int }
      """

      p_
          """We'll need to update our model as well. Fable and Elmish will push
         us to handle errors and deal with data that doesn't exist, so let's
         create a model that can store data about whether we've loaded the data yet."""
      fsharpBlock """
type Model =
    { Loading : bool
      PokemonData : Option<Result<Pokemon, string>> }
"""
      p_ """A method for making the actual web request seems helpful too.
         Let's whack that in as well (put the open statements up at the top
         of the file with the others, and the function before the "init"
         function):"""
      fsharpBlock """
open Fable.PowerPack
open Fable.PowerPack.Fetch

let fetchPokemon pokemonId =
    promise {
        let url = (sprintf "https://pokeapi.co/api/v2/pokemon/%d/" pokemonId)

        let props =
            [ RequestProperties.Method HttpMethod.GET
              requestHeaders [ HttpRequestHeaders.Accept "application/json" ] ]
        return! fetchAs<Pokemon> url props
    }
"""
      p_ """Anything that updates the state of our application has to go via
         our "update" function via a message. Let's rewrite both to know
         about loading things, and configure our starting state:"""
      fsharpBlock """
type Msg =
    | LoadPokemon
    | PokemonLoaded of Pokemon
    | LoadingFailed of string

let init _ =
    let model =
        { Loading = false
          PokemonData = None }
    model, Cmd.ofMsg LoadPokemon

let update msg model =
    match msg with
    | LoadPokemon ->
        { model with Loading = true
                     PokemonData = None },
        // Pick a different random number here between
        // 1 and 949
        Cmd.ofPromise fetchPokemon 10 PokemonLoaded
            (fun e -> LoadingFailed e.Message)
    | PokemonLoaded data ->
        { model with PokemonData = Some(Ok data)
                     Loading = false }, Cmd.none
    | LoadingFailed error ->
        { model with PokemonData = Some(Error error)
                     Loading = false }, Cmd.none
"""
      p_ """The important magic happens in the LoadPokemon route, where we
         wrap the Promise (think Async) returned by our fetch function in
         a Cmd. Cmd is provided by Elmish, and it is one of the two ways
         (along with dispatch) in
         which messages can be fed into the application. A Cmd is a thing
         which will at some later point cause a message to be sent to the
         update function, creating a new state - without any further user
         interaction. If the user is causing the message via the "view",
         that's when we use "dispatch"."""
      p_ """In this case, if the promise reports success, we wrap the result
         in the "PokemonLoaded" message, and fire it back into update. If we
         get an error, we extract the error message and send that back
         instead."""
      p_ """Nearly there! Any Elmish application needs a model, update logic
         and a view. Our view still doesn't compile, so let's update it
         like this:"""
      fsharpBlock """
let view model dispatch =
    let content =
        if model.Loading then [ p [] [ str "Loooading!" ] ]
        else
            match model.PokemonData with
            | Some(Ok data) ->
                [ p [] [ str (sprintf "Name: %s" data.name) ]
                  p [] [ str (sprintf "Weight: %d" data.weight) ] ]
            | Some(Error message) ->
                [ p [] [ str "Oh noes: it went wrong! The error was:" ]
                  p [] [ str message ] ]
            | None -> [ p [] [ str "No Pokemon loaded." ] ]
    Container.container [] [ Content.content [] content ]
"""
      p_ """Hopefully your app has now compiled and is probably already
         displaying some Pokemon data. Reload it to see a brief flash of
         a "Loooading!" message before the request completes.""" ]

let page =
    { Id = BeginningsLoad
      Title = "Loading a Pokemon"
      Subtitle = None
      Content = content
      Requires = Set.ofList [ IntroPokeapi; IntroNewProjectFromTemplate ] }
